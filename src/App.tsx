import React, { FC } from "react";
import Router from "./router/Router";
import GlobalStyles from "./styles/GlobalStyles";
import { ApolloProvider } from "@apollo/client";
import tokopediaClient from "./apis/tokopedia";
import { ContactProvider } from "./modules/contact/contexts/ContactContext";

const App: FC = () => {
  return (
    <>
      <ApolloProvider client={tokopediaClient}>
        <ContactProvider>
          <GlobalStyles />
          <Router />
        </ContactProvider>
      </ApolloProvider>
    </>
  );
};

export default App;
