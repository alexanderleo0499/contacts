import React, { ReactNode } from "react";
import { FC, InputHTMLAttributes } from "react";
import {
  styleContainerInput,
  styleInput,
  styleLabelInput,
  stylePrefixIcon,
  styleSuffixIcon,
} from "./styles";
import { css } from "@emotion/react";

interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
  label?: string;
  SuffixIcon?: ReactNode;
  PrefixIcon?: ReactNode;
}

const Input: FC<InputProps> = ({ label, SuffixIcon, PrefixIcon, ...rest }) => {
  const styleInputUpdated = css([
    styleInput,
    PrefixIcon && { paddingLeft: "2.5rem" },
  ]);
  return (
    <div css={styleContainerInput}>
      {PrefixIcon && <div css={stylePrefixIcon}>{PrefixIcon}</div>}
      {label && (
        <label htmlFor={rest.id} css={styleLabelInput}>
          {label}
        </label>
      )}
      <input {...rest} css={styleInputUpdated} />
      {SuffixIcon && <div css={styleSuffixIcon}>{SuffixIcon}</div>}
    </div>
  );
};

export default Input;
