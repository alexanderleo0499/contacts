import { FC, InputHTMLAttributes } from "react";
import { styleContainerInput, styleInputEmptyDesign } from "./styles";

type InputEmptyDesignProps = InputHTMLAttributes<HTMLInputElement>;

const InputEmptyDesign: FC<InputEmptyDesignProps> = ({ ...rest }) => {
  return (
    <div css={styleContainerInput}>
      <input {...rest} css={styleInputEmptyDesign} />
    </div>
  );
};

export default InputEmptyDesign;
