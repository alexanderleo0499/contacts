import { css } from "@emotion/react";

export const styleContainerInput = css({
  width: "100%",
  position: "relative",
});

export const stylePrefixIcon = css({
  position: "absolute",
  left: "10px",
  top: "50%",
  transform: "translateY(-50%)",
});

export const styleSuffixIcon = css({
  position: "absolute",
  right: "10px",
  top: "50%",
  transform: "translateY(-50%)",
});
export const styleInput = css({
  width: "100%",
  borderRadius: "var(--size-rounded)",
  backgroundColor: "var(--color-slate-100)",
  border: "1px solid var(--color-slate-300)",
  padding: "0.375rem 0.75rem",
  transition: "border var(--time-animation)",
  transitionTimingFunction: "ease-in",
  ":focus": {
    border: "1px solid var(--color-primary)",
    outline: "none",
  },
});

export const styleLabelInput = css({
  color: "var(--color-primary)",
  marginBottom: "0.5rem",
});

export const styleInputEmptyDesign = css({
  width: "100%",
  border: "none",
  ":focus": {
    outline: "none",
  },
});
