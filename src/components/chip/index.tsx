import React, { FC, HTMLAttributes } from "react";
import { styleChip } from "./styles";

interface ChipProps extends HTMLAttributes<HTMLDivElement> {
  isActive?: boolean;
}
const Chip: FC<ChipProps> = ({ isActive, ...rest }) => {
  return (
    <div
      css={styleChip}
      {...rest}
      style={
        isActive
          ? {
              background: "black",
              color: "white",
            }
          : {}
      }
    ></div>
  );
};

export default Chip;
