import { css } from "@emotion/react";

export const styleChip = css({
  borderRadius: "1.4rem",
  padding: "0.3rem 1.2rem",
  border: "1.3px solid var(--color-slate-500)",
  cursor: "pointer",
  fontWeight: "bold",
  fontSize: "0.8rem",
  width: "max-content",
});
