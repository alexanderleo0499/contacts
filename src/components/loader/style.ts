import { css } from "@emotion/react";

export const styleLoaderContainer = css({
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  width: "100%",
  height: "100%",
  position: "fixed",
  fontWeight: "bold",
  top: "0",
  left: "0",
  zIndex: "50",
  color: "white",
  backgroundColor: "rgba(0,0,0,0.7)",
  "@media only screen and (min-width: 800px)": {
    width: "25rem",
    left: "50%",
    transform: "translateX(-50%)",
  },
});
