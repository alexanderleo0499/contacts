import React, { FC, HTMLAttributes } from "react";
import { styleLoaderContainer } from "./style";

interface LoaderProps extends HTMLAttributes<HTMLDivElement> {
  isLoading?: boolean;
}
const Loader: FC<LoaderProps> = ({ isLoading, ...rest }) => {
  if (!isLoading) return <></>;
  return <div css={styleLoaderContainer} {...rest}></div>;
};

export default Loader;
