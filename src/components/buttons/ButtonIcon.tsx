import React, { ButtonHTMLAttributes, FC, ReactNode } from "react";
import { styleButtonIconContainer } from "./styles";

interface ButtonIconProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  Icon: ReactNode;
}
const ButtonIcon: FC<ButtonIconProps> = ({ Icon, ...rest }) => {
  return (
    <button {...rest} css={styleButtonIconContainer}>
      {Icon}
    </button>
  );
};

export default ButtonIcon;
