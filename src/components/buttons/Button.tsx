import React, { ButtonHTMLAttributes, FC } from "react";
import { styleButton } from "./styles";

type ButtonProps = ButtonHTMLAttributes<HTMLButtonElement>;
const Button: FC<ButtonProps> = ({ ...rest }) => {
  return <button {...rest} css={styleButton}></button>;
};

export default Button;
