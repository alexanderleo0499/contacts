import { css } from "@emotion/react";

export const styleButtonIconContainer = css({
  cursor: "pointer",
  border: "none",
  backgroundColor: "transparent",
  padding: "0.3rem",
});

export const styleButton = css({
  cursor: "pointer",
  border: "1.5px solid var(--color-slate-300)",
  borderRadius: "0.25rem",
  backgroundColor: "white",
  fontWeight: "bold",
  width: "100%",
  textAlign: "center",
  padding: "0.3rem 0.5rem",
});
