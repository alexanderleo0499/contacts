import React, { FC, HTMLAttributes } from "react";
import { styleList } from "./styles";

type ListProps = HTMLAttributes<HTMLDivElement>;
const List: FC<ListProps> = ({ ...rest }) => {
  return <div {...rest} css={styleList}></div>;
};

export default List;
