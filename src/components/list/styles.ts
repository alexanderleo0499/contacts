import { css } from "@emotion/react";

export const styleList = css({
  borderBottom: "1px solid var(--color-slate-200)",
  padding: "0.5rem 0",
  cursor: "pointer",
});
