import { css } from "@emotion/react";

export const styleModalBackdrop = css({
  backgroundColor: "rgba(0,0,0,0.8)",
  height: "100%",
  width: "100%",
  position: "fixed",
  zIndex: 10,
  top: 0,
  left: 0,
  cursor: "pointer",
  "@media only screen and (min-width: 800px)": {
    width: "25rem",
    left: "50%",
    transform: "translateX(-50%)",
  },
});

export const styleModalBody = css({
  backgroundColor: "white",
  height: "100%",
  width: "100%",
  position: "fixed",
  zIndex: 20,
  top: 0,
  left: 0,
  display: "flex",
  flexDirection: "column",
  "@media only screen and (min-width: 800px)": {
    width: "25rem",
    left: "50%",
    transform: "translateX(-50%)",
  },
});

export const styleBar = css({
  backgroundColor: "white",
  boxShadow: "var(--shadow)",
  display: "flex",
  alignItems: "center",
  justifyContent: "space-between",
  padding: "0.6rem 0.8rem",
  fontWeight: "bold",
  flex: "var(--flex-initial)",
});

export const styleContentModal = css({
  overflowY: "auto",
  flex: "var(--flex-1)",
});
