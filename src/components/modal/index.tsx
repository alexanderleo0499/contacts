import React, { FC, HTMLAttributes, ReactNode } from "react";
import {
  styleBar,
  styleContentModal,
  styleModalBackdrop,
  styleModalBody,
} from "./style";
import ButtonIcon from "../buttons/ButtonIcon";
import { X } from "lucide-react";

interface ModalProps extends HTMLAttributes<HTMLDivElement> {
  titleBar: ReactNode;
  isOpen?: boolean;
  handleCloseModal: () => void;
}
const Modal: FC<ModalProps> = ({
  isOpen,
  titleBar,
  children,
  handleCloseModal,
  ...rest
}) => {
  if (!isOpen) return <></>;
  return (
    <>
      <div css={styleModalBackdrop}></div>
      <div css={styleModalBody}>
        <div css={styleBar}>
          {titleBar}
          <ButtonIcon Icon={<X size={20} />} onClick={handleCloseModal} />
        </div>
        <div css={styleContentModal} {...rest}>
          {children}
        </div>
      </div>
    </>
  );
};

export default Modal;
