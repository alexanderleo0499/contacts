import React, { FC, HTMLAttributes } from "react";
import { styleTextSubtitle, styleTextTitle } from "./style";

type TextProps = HTMLAttributes<HTMLDivElement>;

export const TextTitle: FC<TextProps> = ({ ...rest }) => {
  return <div {...rest} css={styleTextTitle}></div>;
};
export const TextSubtitle: FC<TextProps> = ({ ...rest }) => {
  return <div {...rest} css={styleTextSubtitle}></div>;
};
