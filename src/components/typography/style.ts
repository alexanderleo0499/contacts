import { css } from "@emotion/react";

export const styleTextTitle = css({
  fontWeight: "bold",
  fontSize: "1rem",
});

export const styleTextSubtitle = css({
  fontSize: "0.8rem",
  color: "var(--color-slate-500)",
});
