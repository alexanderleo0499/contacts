import { calculateCeilTotalPaginateItem } from "@src/lib/utils";
import { ArrowLeft, ArrowRight } from "lucide-react";
import React, { useEffect, useState } from "react";
import {
  styleBtnPagination,
  styleBtnTextPagination,
  stylePaginationContainer,
} from "./styles";
import ButtonIcon from "../buttons/ButtonIcon";

interface PaginateProps {
  className?: string;
  page: number;
  pageSize: number;
  totalItem: number;
  onChangePage: (page: number) => void;
}

const Paginate: React.FC<PaginateProps> = ({
  className,
  onChangePage,
  page,
  pageSize,
  totalItem,
}) => {
  const [ceilTotalItem, setCeilTotalItem] = useState<number | null>(null);

  useEffect(() => {
    setCeilTotalItem(calculateCeilTotalPaginateItem(totalItem, pageSize));
  }, [totalItem, pageSize]);

  const RenderPageNumbers: React.FC<{ total: number; start?: number }> = ({
    total,
    start = 0,
  }) => {
    return (
      <div>
        {[...Array(total)].map((_, idx) => {
          const pageNumber: number = start + idx + 1;
          return (
            <button
              // className={`border border-primary p-2  ${
              //   page === pageNumber ? "bg-primary text-white" : ""
              // }`}
              css={styleBtnPagination}
              style={
                page === pageNumber
                  ? {
                      backgroundColor: "var(--color-primary)",
                      color: "white",
                    }
                  : {}
              }
              key={pageNumber}
              role="pagination-page-btn"
              onClick={() => onChangePage(pageNumber)}
            >
              <div css={styleBtnTextPagination}>{pageNumber}</div>
            </button>
          );
        })}
      </div>
    );
  };

  if (totalItem === 0 || !totalItem) {
    return <></>;
  }

  return (
    <div css={stylePaginationContainer} data-testid="pagination-container">
      <ButtonIcon
        disabled={page === 1}
        role="pagination-left-btn"
        onClick={() => onChangePage(page - 1)}
        Icon={<ArrowLeft size={15} />}
      ></ButtonIcon>
      {ceilTotalItem && ceilTotalItem <= 8 && (
        <RenderPageNumbers total={ceilTotalItem} />
      )}
      {ceilTotalItem && ceilTotalItem > 8 && (
        <>
          <RenderPageNumbers
            total={page > 5 ? 3 : page < 3 ? 3 : 3 + (page - 2)}
          />
          {page > 5 && <div style={{ marginTop: "0.25rem" }}>...</div>}
          {page > 5 && page <= ceilTotalItem - 5 && (
            <RenderPageNumbers start={page - 2} total={3} />
          )}
          {page < ceilTotalItem - 4 && (
            <div style={{ marginTop: "0.25rem" }}>...</div>
          )}
          <RenderPageNumbers
            total={
              ceilTotalItem - page > 4
                ? 3
                : page === ceilTotalItem
                ? ceilTotalItem - page + 3
                : ceilTotalItem - page + 2
            }
            start={
              ceilTotalItem - page > 4
                ? ceilTotalItem - 3
                : page === ceilTotalItem
                ? page - 3
                : page - 2
            }
          />
        </>
      )}

      <ButtonIcon
        role="pagination-right-btn"
        disabled={page === ceilTotalItem}
        onClick={() => onChangePage(page + 1)}
        Icon={<ArrowRight size={15} />}
      ></ButtonIcon>
    </div>
  );
};
export default Paginate;
