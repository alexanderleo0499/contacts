import { css } from "@emotion/react";

export const styleBtnPagination = css({
  border: "1px solid var(--color-primary)",
  padding: "0.5rem",
  cursor: "pointer",
  background: "white",
});

export const styleBtnTextPagination = css({
  width: "1rem",
  height: "1rem",
  marginTop: "-0.25rem",
  fontSize: "1rem",
});

export const stylePaginationContainer = css({
  display: "flex",
  "> * + *": {
    marginLeft: "0.25rem",
  },
});
