import { KEY_FAVORITE_CONTACTS_TOKOPEDIA } from "@src/lib/constants/localstorage";
import {
  Dispatch,
  FC,
  ReactNode,
  SetStateAction,
  createContext,
  useState,
} from "react";
import { Contact, FormContact } from "../typings/contact";

export type SearchBy = "firstName" | "lastName" | "phone";

export interface ContactContextType {
  renderedContacts: Contact[];
  setRenderedContacts: Dispatch<SetStateAction<Contact[]>>;
  favoriteContactIds: number[];
  favoriteContacts: Contact[];
  setFavoriteContacts: Dispatch<SetStateAction<Contact[]>>;
  handleAddFavoriteContact: (idContact: number) => void;
  handleRemoveFavoriteContact: (idContact: number) => void;
  openFormContact: FormContact | null;
  setOpenFormContact: Dispatch<SetStateAction<FormContact | null>>;
  searchBy: SearchBy;
  setSearchBy: Dispatch<SetStateAction<SearchBy>>;
  openModalFilter: boolean;
  setOpenModalFilter: Dispatch<SetStateAction<boolean>>;
}

interface ContactContextProviderProps {
  children: ReactNode;
}

export const ContactContext = createContext<ContactContextType | null>(null);

const persistedRawFav = localStorage.getItem(KEY_FAVORITE_CONTACTS_TOKOPEDIA);
const persistedParsedFav = persistedRawFav ? JSON.parse(persistedRawFav) : [];

export const FORM_CONTACT_INITIAL_STATE = {
  firstName: "",
  lastName: "",
  phones: [],
};

export const ContactProvider: FC<ContactContextProviderProps> = ({
  children,
}) => {
  const [favoriteContactIds, setFavoriteContactIds] =
    useState<number[]>(persistedParsedFav);

  const [openFormContact, setOpenFormContact] = useState<FormContact | null>(
    null
  );

  const [renderedContacts, setRenderedContacts] = useState<Contact[]>([]);
  const [favoriteContacts, setFavoriteContacts] = useState<Contact[]>([]);
  const [searchBy, setSearchBy] = useState<SearchBy>("firstName");
  const [openModalFilter, setOpenModalFilter] = useState<boolean>(false);

  const handleAddFavoriteContact = (idContact: number) => {
    const copiedFav = [...favoriteContactIds];
    copiedFav.push(idContact);
    localStorage.setItem(
      KEY_FAVORITE_CONTACTS_TOKOPEDIA,
      JSON.stringify(copiedFav)
    );
    setFavoriteContactIds(copiedFav);

    //update favorite state
    if (!openFormContact) return;
    const copiedSelectedContact = { ...openFormContact };
    copiedSelectedContact.isFavorite = true;
    setOpenFormContact(copiedSelectedContact);
  };

  const handleRemoveFavoriteContact = (idContact: number) => {
    const copiedFav = [...favoriteContactIds];
    const updatedFav = copiedFav.filter((id) => id !== idContact);

    localStorage.setItem(
      KEY_FAVORITE_CONTACTS_TOKOPEDIA,
      JSON.stringify(updatedFav)
    );
    setFavoriteContactIds(updatedFav);

    //remove favorite state
    if (!openFormContact) return;
    const copiedSelectedContact = { ...openFormContact };
    copiedSelectedContact.isFavorite = false;
    setOpenFormContact(copiedSelectedContact);
  };
  return (
    <ContactContext.Provider
      value={{
        favoriteContactIds,
        handleAddFavoriteContact,
        handleRemoveFavoriteContact,
        openFormContact,
        setOpenFormContact,
        renderedContacts,
        setRenderedContacts,
        favoriteContacts,
        setFavoriteContacts,
        openModalFilter,
        searchBy,
        setOpenModalFilter,
        setSearchBy,
      }}
    >
      {children}
    </ContactContext.Provider>
  );
};
