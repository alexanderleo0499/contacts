import { useLazyQuery } from "@apollo/client";
import { useContext, useEffect } from "react";
import {
  Contact,
  ContactWhereParams,
  FetchContactData,
} from "../typings/contact";
import { GetContactList } from "../graphql/queries";
import useDebounceSearch from "@src/lib/hooks/useDebouncedSearch";
import { ContactContext, ContactContextType } from "../contexts/ContactContext";

const useFetchContact = () => {
  const { usedSearch, search, setSearch } = useDebounceSearch(600);
  const {
    favoriteContactIds,
    setRenderedContacts,
    setFavoriteContacts,
    searchBy,
  } = useContext(ContactContext) as ContactContextType;

  const [getContactList, { loading, error, data }] =
    useLazyQuery<FetchContactData>(GetContactList);

  //fetch
  useEffect(() => {
    if (!usedSearch) {
      getContactList({
        variables: {
          order_by: {
            first_name: "asc",
          },
        },
        fetchPolicy: "network-only",
      });
      return;
    }

    const searchObject: ContactWhereParams = {};
    if (searchBy === "firstName")
      searchObject.first_name = { _like: usedSearch };
    if (searchBy === "lastName") searchObject.last_name = { _like: usedSearch };
    if (searchBy === "phone")
      searchObject.phones = { number: { _like: usedSearch } };
    getContactList({
      variables: {
        where: {
          ...searchObject,
        },
        order_by: {
          first_name: "asc",
        },
      },
      fetchPolicy: "network-only",
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [usedSearch]);

  //update render contacts
  useEffect(() => {
    if (!data) return;
    const regulerContacts: Contact[] = data.contact.filter(
      (c) => !favoriteContactIds.includes(c.id)
    );
    const favoriteContacts: Contact[] = data.contact
      .filter((c) => favoriteContactIds.includes(c.id))
      .map((c) => {
        return { ...c, isFavorite: true };
      });

    setFavoriteContacts(favoriteContacts);
    setRenderedContacts(regulerContacts);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [favoriteContactIds, data]);

  return {
    search,
    setSearch,
    loading,
    error,
    getContactList,
  };
};
export default useFetchContact;
