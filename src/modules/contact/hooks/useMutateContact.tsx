import { useContext, useState } from "react";
import { Contact, FetchContactData, FormContact } from "../typings/contact";
import tokopediaClient from "@src/apis/tokopedia";
import {
  AddContactWithPhones,
  AddNumberToContact,
  DeleteContactById,
  EditContactById,
  EditPhoneNumber,
} from "../graphql/mutations";
import { ContactContext, ContactContextType } from "../contexts/ContactContext";
import { GetContactList } from "../graphql/queries";
import { findUniqueObjectBetweenSameArrayModel } from "@src/lib/utils";

const useMutateContact = () => {
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<string>("");

  const {
    setOpenFormContact,
    setRenderedContacts,
    openFormContact,
    favoriteContactIds,
    setFavoriteContacts,
  } = useContext(ContactContext) as ContactContextType;

  const handleRefetch = async () => {
    const res = await tokopediaClient.query<FetchContactData>({
      query: GetContactList,
      variables: {
        order_by: {
          first_name: "asc",
        },
      },
      fetchPolicy: "network-only",
    });
    const regulerContacts: Contact[] = res.data.contact.filter(
      (c) => !favoriteContactIds.includes(c.id)
    );
    const favoriteContacts: Contact[] = res.data.contact
      .filter((c) => favoriteContactIds.includes(c.id))
      .map((c) => {
        return { ...c, isFavorite: true };
      });
    setFavoriteContacts(favoriteContacts);
    setRenderedContacts(regulerContacts);
  };

  const handleSubmitAddContact = async (contact: FormContact) => {
    try {
      setLoading(true);
      setError("");
      if (!contact.firstName) throw new Error("First name is required");
      if (!contact.phones[0]) throw new Error("Phone number is required");
      //create contact
      await tokopediaClient.mutate({
        mutation: AddContactWithPhones,
        variables: {
          first_name: contact.firstName,
          last_name: contact.lastName,
          phones: contact.phones,
        },
      });

      //refetch
      await handleRefetch();
      setOpenFormContact(null);
    } catch (err: unknown) {
      if (err instanceof Error) {
        return setError(err.message);
      }
      setError("Server Error");
    } finally {
      setLoading(false);
    }
  };

  const handleUpdateContact = async (contact: FormContact) => {
    try {
      setLoading(true);
      setError("");
      if (!contact.firstName) throw new Error("First name is required");
      if (!contact.phones[0]) throw new Error("Phone number is required");
      if (!contact.id || !openFormContact) throw new Error("id is required");

      //update contact phone
      await openFormContact.phones.forEach(async (p, idx) => {
        if (p.number === contact.phones[idx].number) return;
        await tokopediaClient.mutate({
          mutation: EditPhoneNumber,
          variables: {
            pk_columns: {
              number: p.number,
              contact_id: contact.id,
            },
            new_phone_number: contact.phones[idx].number,
          },
        });
      });

      //add contact phone
      if (contact.phones.length > openFormContact.phones.length) {
        const foundUniquePhone = findUniqueObjectBetweenSameArrayModel(
          contact.phones,
          openFormContact.phones
        );
        await tokopediaClient.mutate({
          mutation: AddNumberToContact,
          variables: {
            contact_id: contact.id,
            phone_number: foundUniquePhone[0].number,
          },
        });
      }

      //update data name
      await tokopediaClient.mutate({
        mutation: EditContactById,
        variables: {
          id: contact.id,
          _set: {
            first_name: contact.firstName,
            last_name: contact.lastName,
          },
        },
      });

      //refetch
      await handleRefetch();

      setOpenFormContact(null);
    } catch (err) {
      if (err instanceof Error) {
        return setError(err.message);
      }
      setError("Server Error");
    } finally {
      setLoading(false);
    }
  };

  const handleDeleteContact = async (idContact: number) => {
    try {
      setLoading(true);
      setError("");
      await tokopediaClient.mutate({
        mutation: DeleteContactById,
        variables: {
          id: idContact,
        },
      });
      //refetch
      await handleRefetch();

      setOpenFormContact(null);
    } catch (err: any) {
      if (err instanceof Error) {
        return setError(err.message);
      }
      setError("Server Error");
    } finally {
      setLoading(false);
    }
  };

  return {
    loading,
    error,
    handleSubmitAddContact,
    handleUpdateContact,
    handleDeleteContact,
  };
};

export default useMutateContact;
