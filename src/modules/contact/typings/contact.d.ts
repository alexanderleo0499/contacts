export interface ContactPhone {
  number: string;
}
export interface Contact {
  first_name: string;
  created_at: string;
  id: number;
  last_name: string;
  phones: ContactPhone[];
  isFavorite?: boolean;
}

export interface FetchContactData {
  contact: Contact[];
}

export interface FormContact {
  id?: number;
  firstName: string;
  lastName: string;
  phones: ContactPhone[];
  isFavorite?: boolean;
}

export interface LikeParam {
  _like: string;
}
export interface ContactWhereParams {
  first_name?: LikeParam;
  last_name?: LikeParam;
  phones?: {
    number?: LikeParam;
  };
}
