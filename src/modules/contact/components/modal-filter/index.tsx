import Modal from "@src/components/modal";
import React, { FC, HTMLAttributes, useContext } from "react";
import {
  ContactContext,
  ContactContextType,
} from "../../contexts/ContactContext";
import { styleContainerChip, styleContainerFilter } from "./styles";
import { TextSubtitle } from "@src/components/typography/Text";
import Chip from "@src/components/chip";
import Button from "@src/components/buttons/Button";

type ModalFilterProps = HTMLAttributes<HTMLDivElement>;
const ModalFilter: FC<ModalFilterProps> = () => {
  const { openModalFilter, setOpenModalFilter, setSearchBy, searchBy } =
    useContext(ContactContext) as ContactContextType;
  return (
    <Modal
      isOpen={openModalFilter}
      handleCloseModal={() => setOpenModalFilter(false)}
      titleBar={<div>Filter Settings</div>}
    >
      <div css={styleContainerFilter}>
        <div>
          <TextSubtitle>Filter By</TextSubtitle>
          <div css={styleContainerChip}>
            <Chip
              isActive={searchBy === "firstName"}
              onClick={() => setSearchBy("firstName")}
            >
              First Name
            </Chip>
            <Chip
              isActive={searchBy === "lastName"}
              onClick={() => setSearchBy("lastName")}
            >
              Last Name
            </Chip>
            <Chip
              isActive={searchBy === "phone"}
              onClick={() => setSearchBy("phone")}
            >
              Phone
            </Chip>
          </div>
        </div>
        <Button
          style={{
            background: "black",
            color: "white",
            border: "none",
          }}
          onClick={() => setOpenModalFilter(false)}
        >
          Apply
        </Button>
      </div>
    </Modal>
  );
};

export default ModalFilter;
