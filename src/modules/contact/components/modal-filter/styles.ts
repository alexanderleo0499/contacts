import { css } from "@emotion/react";

export const styleContainerFilter = css({
  padding: "1rem",
  display: "flex",
  flexDirection: "column",
  justifyContent: "space-between",
  height: "98%",
  "> * + *": {
    marginTop: "0.4rem",
  },
});

export const styleContainerChip = css({
  display: "flex",
  "> * + *": {
    marginLeft: "0.5rem",
  },
  overflow: "auto",
});
