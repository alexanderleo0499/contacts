import { css } from "@emotion/react";

export const styleContainerCardProfile = css({
  display: "flex",
  alignItems: "center",
  "> * + *": {
    marginLeft: "1rem",
  },
});

export const stylePhotoProfile = css({
  borderRadius: "100%",
  width: "3.5rem",
  height: "3.5rem",
});
