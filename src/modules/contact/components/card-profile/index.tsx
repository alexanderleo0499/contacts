import List from "@src/components/list";
import React, { FC, HTMLAttributes } from "react";
import { styleContainerCardProfile } from "./styles";
import PhotoProfile from "./PhotoProfile";
import { TextSubtitle, TextTitle } from "@src/components/typography/Text";
import { EMAIL_PROFILE, NAME_PROFILE } from "@src/lib/constants";

interface CardProfileProps extends HTMLAttributes<HTMLDivElement> {
  s?: string;
}
const CardProfile: FC<CardProfileProps> = () => {
  return (
    <List>
      <div css={styleContainerCardProfile}>
        <PhotoProfile />
        <div>
          <TextTitle>{NAME_PROFILE}</TextTitle>
          <TextSubtitle>{EMAIL_PROFILE}</TextSubtitle>
        </div>
      </div>
    </List>
  );
};

export default CardProfile;
