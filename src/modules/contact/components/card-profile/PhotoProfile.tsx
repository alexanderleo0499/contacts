import { imgProfile } from "@src/lib/constants/assets";
import React, { FC, HTMLAttributes } from "react";
import { stylePhotoProfile } from "./styles";

type PhotoProfileProps = HTMLAttributes<HTMLDivElement>;
const PhotoProfile: FC<PhotoProfileProps> = () => {
  return <img src={imgProfile} alt="" css={stylePhotoProfile} />;
};

export default PhotoProfile;
