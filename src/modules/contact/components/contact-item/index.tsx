import List from "@src/components/list";
import React, { FC, HTMLAttributes } from "react";
import { Star } from "lucide-react";
import { styleContainerContactItem, styleStarContactItem } from "./style";

interface ContactItemProps extends HTMLAttributes<HTMLDivElement> {
  isFavorite?: boolean;
}

const ContactItem: FC<ContactItemProps> = ({
  isFavorite,
  children,
  ...rest
}) => {
  return (
    <List>
      <div {...rest} css={styleContainerContactItem}>
        <div>{children}</div>
        {isFavorite && <Star size={15} css={styleStarContactItem} />}
      </div>
    </List>
  );
};

export default ContactItem;
