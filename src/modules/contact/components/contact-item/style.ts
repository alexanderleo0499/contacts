import { css } from "@emotion/react";

export const styleContainerContactItem = css({
  display: "flex",
  justifyContent: "space-between",
  alignItems: "center",
});

export const styleStarContactItem = css({
  color: "var(--color-slate-500)",
});
