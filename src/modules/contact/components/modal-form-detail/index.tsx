import Modal from "@src/components/modal";
import React, { FC, HTMLAttributes, useContext, useState } from "react";
import {
  ContactContext,
  ContactContextType,
} from "../../contexts/ContactContext";
import FormContactDetail from "./FormContactDetail";
import { styleTitleBarContact } from "./styles";
import { Star } from "lucide-react";
import Loader from "@src/components/loader";

type ModalFormDetailProps = HTMLAttributes<HTMLDivElement>;
const ModalFormDetail: FC<ModalFormDetailProps> = () => {
  const { openFormContact, setOpenFormContact } = useContext(
    ContactContext
  ) as ContactContextType;

  const isUpdateData = openFormContact?.firstName ? true : false;

  return (
    <Modal
      titleBar={
        <div css={styleTitleBarContact}>
          {isUpdateData ? (
            <div>
              {openFormContact?.firstName} {openFormContact?.lastName}
            </div>
          ) : (
            <div>Create New Contact</div>
          )}
          {openFormContact?.isFavorite && <Star size={15} />}
        </div>
      }
      isOpen={openFormContact ? true : false}
      handleCloseModal={() => setOpenFormContact(null)}
    >
      {openFormContact && (
        <FormContactDetail
          contactData={openFormContact}
          isUpdateData={isUpdateData}
        />
      )}
      <Loader />
    </Modal>
  );
};

export default ModalFormDetail;
