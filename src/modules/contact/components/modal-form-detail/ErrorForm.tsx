import React, { FC, HTMLAttributes } from "react";
import { styleErrorForm } from "./styles";

type ErrorFormProps = HTMLAttributes<HTMLDivElement>;
const ErrorForm: FC<ErrorFormProps> = ({ children, ...rest }) => {
  if (!children) return <></>;
  return (
    <div css={styleErrorForm} {...rest}>
      {children}
    </div>
  );
};

export default ErrorForm;
