import { css } from "@emotion/react";

export const styleFormContactDetail = css({
  padding: "1rem",
  "> * + *": {
    marginTop: "1rem",
  },
});

export const styleTitleBarContact = css({
  display: "flex",
  alignItems: "center",
  "> * + *": {
    marginLeft: "0.5rem",
  },
});

export const styleButtonList = css({
  display: "flex",
  alignItems: "center",
  fontWeight: "bold",
  fontSize: "0.9rem",
  "> * + *": {
    marginLeft: "0.5rem",
  },
});

export const styleErrorForm = css({
  color: "var(--color-red-500)",
  fontWeight: "bold",
  textAlign: "center",
});
