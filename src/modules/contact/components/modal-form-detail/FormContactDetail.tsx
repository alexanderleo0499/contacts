import React, { FC, FormHTMLAttributes, useContext, useState } from "react";
import { ContactPhone, FormContact } from "../../typings/contact";
import Input from "@src/components/input";
import { styleButtonList, styleFormContactDetail } from "./styles";
import List from "@src/components/list";
import InputEmptyDesign from "@src/components/input/InputEmptyDesign";
import { Plus } from "lucide-react";
import Button from "@src/components/buttons/Button";
import {
  ContactContext,
  ContactContextType,
} from "../../contexts/ContactContext";
import useMutateContact from "../../hooks/useMutateContact";
import ErrorForm from "./ErrorForm";
import {
  styleFlex1,
  styleFlexInitial,
  styleFlexItemCenter,
} from "@src/styles/shared.styles";
import Loader from "@src/components/loader";
import { css } from "@emotion/react";

interface FormContactDetailProps extends FormHTMLAttributes<HTMLFormElement> {
  contactData: FormContact;
  isUpdateData: boolean;
}
const FormContactDetail: FC<FormContactDetailProps> = ({
  contactData,
  isUpdateData,
  ...rest
}) => {
  const { handleRemoveFavoriteContact, handleAddFavoriteContact } = useContext(
    ContactContext
  ) as ContactContextType;

  const {
    error,
    handleSubmitAddContact,
    loading,
    handleUpdateContact,
    handleDeleteContact,
  } = useMutateContact();

  //form states
  const [firstName, setFirstName] = useState<string>(contactData.firstName);
  const [lastName, setLastName] = useState<string>(contactData.lastName);
  const [phones, setPhones] = useState<ContactPhone[]>(
    isUpdateData ? contactData.phones : [{ number: "" }]
  );

  //compued states
  const canAddNewPhone =
    contactData.phones.length === phones.length ? true : false;

  //functions
  const handleAddNewPhone = () => {
    const copiedPhones = [...phones];
    copiedPhones.push({ number: "" });
    setPhones(copiedPhones);
  };

  return (
    <form
      {...rest}
      css={styleFormContactDetail}
      onSubmit={(e) => {
        e.preventDefault();
        //update
        if (isUpdateData) {
          return handleUpdateContact({
            firstName,
            lastName,
            phones,
            id: contactData.id,
          });
        }
        //add new
        handleSubmitAddContact({
          firstName,
          lastName,
          phones,
        });
      }}
    >
      <Loader isLoading={loading}>
        {isUpdateData ? "Updating Contact" : "Creating Contact"}...
      </Loader>
      <Input
        label="First Name"
        placeholder="Ex. John"
        value={firstName}
        onChange={(e) => setFirstName(e.target.value)}
      />
      <Input
        label="Last Name"
        placeholder="Ex. Doe"
        value={lastName}
        onChange={(e) => setLastName(e.target.value)}
      />
      <div>
        <div>Phones</div>
        {phones.map((p, idx) => {
          return (
            <List key={idx}>
              <div css={styleFlexItemCenter}>
                <span css={styleFlexInitial}>+62 |</span>
                <div css={styleFlex1}>
                  <InputEmptyDesign
                    defaultValue={p.number}
                    placeholder="Phone"
                    onChange={(e) => {
                      const copiedPhones = [...phones];
                      const copiedPhone = { ...phones[idx] };
                      copiedPhone.number = e.target.value;
                      copiedPhones[idx] = copiedPhone;
                      setPhones(copiedPhones);
                    }}
                  />
                </div>
              </div>
            </List>
          );
        })}
        {canAddNewPhone && (
          <List>
            <div css={styleButtonList} onClick={handleAddNewPhone}>
              <Plus size={15} />
              <div>add phone</div>
            </div>
          </List>
        )}
      </div>

      <Button>{isUpdateData ? "Save Changes" : "Create Contact"}</Button>
      {isUpdateData && (
        <>
          {contactData.isFavorite && (
            <Button
              onClick={() => handleRemoveFavoriteContact(contactData.id || 0)}
              type="button"
            >
              Remove From Favorites
            </Button>
          )}
          {!contactData.isFavorite && (
            <Button
              onClick={() => handleAddFavoriteContact(contactData.id || 0)}
              type="button"
            >
              Add To Favorites
            </Button>
          )}
        </>
      )}
      {isUpdateData && (
        <Button
          onClick={() => {
            if (!contactData.id) return;
            handleDeleteContact(contactData.id);
          }}
        >
          <div
            css={css`
              color: var(--color-red-500);
            `}
          >
            Remove Contact
          </div>
        </Button>
      )}
      <ErrorForm>{error}</ErrorForm>
    </form>
  );
};

export default FormContactDetail;
