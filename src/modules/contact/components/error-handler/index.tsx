import React, { FC, HTMLAttributes } from "react";
import { Contact } from "../../typings/contact";
import { styleErrorHandler } from "./style";
import { ApolloError } from "@apollo/client";

interface ErrorHandlerProps extends HTMLAttributes<HTMLDivElement> {
  contacts?: Contact[];
  favoriteContacts: Contact[];
  error?: ApolloError;
}
const ErrorHandler: FC<ErrorHandlerProps> = ({
  favoriteContacts,
  contacts,
  error,
  ...rest
}) => {
  if (error)
    return (
      <div {...rest} css={styleErrorHandler}>
        {error.message}
      </div>
    );
  if (!contacts?.length && !favoriteContacts.length)
    return (
      <div {...rest} css={styleErrorHandler}>
        Search not found
      </div>
    );
  return <></>;
};

export default ErrorHandler;
