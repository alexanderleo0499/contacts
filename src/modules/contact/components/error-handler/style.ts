import { css } from "@emotion/react";

export const styleErrorHandler = css({
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  marginTop: "30vh",
});
