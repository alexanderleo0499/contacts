import { css } from "@emotion/react";

export const styleContainerContactPage = css({
  padding: "1rem",
  paddingTop: "4rem",
});

export const styleContainerHeaderContact = css({
  display: "flex",
  alignItems: "center",
  position: "fixed",
  width: "100%",
  top: "0",
  left: "0",
  backgroundColor: "white",
  padding: "0.7rem",
  "> * + *": {
    marginLeft: "0.3rem",
  },
  "@media only screen and (min-width: 800px)": {
    width: "25rem",
    left: "50%",
    transform: "translateX(-50%)",
  },
});

export const styleContactContainer = css({
  marginTop: "0.5rem",
});

export const styleContainerList = css({
  marginTop: "1rem",
  "> * + *": {
    marginTop: "1rem",
  },
});
