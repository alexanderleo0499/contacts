import React, { useContext } from "react";
import Input from "@src/components/input";
import { FC } from "react";
import { Filter, FilterIcon, Plus, Search } from "lucide-react";
import {
  styleContactContainer,
  styleContainerContactPage,
  styleContainerHeaderContact,
  styleContainerList,
} from "./styles";
import ButtonIcon from "@src/components/buttons/ButtonIcon";
import CardProfile from "./components/card-profile";
import useFetchContact from "./hooks/useFetchContact";
import ContactItem from "./components/contact-item";
import ErrorHandler from "./components/error-handler";
import {
  ContactContext,
  ContactContextType,
  FORM_CONTACT_INITIAL_STATE,
} from "./contexts/ContactContext";
import ModalFormDetail from "./components/modal-form-detail";
import { TextSubtitle } from "@src/components/typography/Text";
import Paginate from "@src/components/paginate";
import { styleFlexItemCenter } from "@src/styles/shared.styles";
import useFilterState from "@src/lib/hooks/useFilterState";
import { INITIAL_PAGE_SIZE } from "@src/lib/constants";
import { Contact } from "./typings/contact";
import ModalFilter from "./components/modal-filter";
import { renderPlaceholderText } from "./utils";

const ContactPage: FC = () => {
  const { search, setSearch, loading, error } = useFetchContact();
  const {
    setOpenFormContact,
    renderedContacts: contacts,
    favoriteContacts,
    setOpenModalFilter,
    searchBy,
  } = useContext(ContactContext) as ContactContextType;

  const { selectedPage, setSelectedPage, renderedData } = useFilterState({
    srcData: contacts,
    isPagination: true,
  });

  const RenderListContact = () => {
    return (
      <div css={styleContainerList}>
        {favoriteContacts.length > 0 && (
          <div>
            <TextSubtitle>Favorite Contacts</TextSubtitle>
            {favoriteContacts.map((c) => {
              return (
                <ContactItem
                  key={c.id}
                  isFavorite={c.isFavorite}
                  onClick={() =>
                    setOpenFormContact({
                      firstName: c.first_name,
                      lastName: c.last_name,
                      phones: c.phones,
                      id: c.id,
                      isFavorite: c.isFavorite,
                    })
                  }
                >
                  {c.first_name} {c.last_name}
                </ContactItem>
              );
            })}
          </div>
        )}
        {contacts.length > 0 && (
          <div>
            <TextSubtitle>Regular Contacts</TextSubtitle>
            {renderedData.map((c: Contact) => {
              return (
                <ContactItem
                  key={c.id}
                  isFavorite={c.isFavorite}
                  onClick={() =>
                    setOpenFormContact({
                      firstName: c.first_name,
                      lastName: c.last_name,
                      phones: c.phones,
                      id: c.id,
                      isFavorite: c.isFavorite,
                    })
                  }
                >
                  {c.first_name} {c.last_name}
                </ContactItem>
              );
            })}
          </div>
        )}
      </div>
    );
  };

  return (
    <>
      <div css={styleContainerContactPage}>
        <div css={styleContainerHeaderContact}>
          <Input
            placeholder={renderPlaceholderText(searchBy)}
            PrefixIcon={<Search size={20} />}
            value={search}
            onChange={(e) => setSearch(e.target.value)}
          />
          <ButtonIcon
            Icon={<Filter size={25} />}
            onClick={() => setOpenModalFilter(true)}
          />
          <ButtonIcon
            Icon={<Plus size={25} />}
            onClick={() => setOpenFormContact(FORM_CONTACT_INITIAL_STATE)}
          />
        </div>
        <CardProfile />
        <RenderListContact />
        <div css={styleFlexItemCenter} style={{ marginTop: "1rem" }}>
          <Paginate
            page={selectedPage}
            pageSize={INITIAL_PAGE_SIZE}
            totalItem={contacts.length}
            onChangePage={(val) => setSelectedPage(val)}
          />
        </div>
        {/* ERROR HANDLING */}
        {contacts.length <= 0 && favoriteContacts.length <= 0 && (
          <ErrorHandler
            error={error}
            contacts={contacts}
            favoriteContacts={favoriteContacts}
          />
        )}
        <div css={styleContactContainer}></div>
      </div>
      <ModalFormDetail />
      <ModalFilter />
    </>
  );
};
export default ContactPage;
