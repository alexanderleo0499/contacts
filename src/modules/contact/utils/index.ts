import { SearchBy } from "../contexts/ContactContext";

export const renderPlaceholderText = (searchBy: SearchBy) => {
  if (searchBy === "firstName") return "Search First Name";
  if (searchBy === "lastName") return "Search Last Name";
  return "Search Phone";
};
