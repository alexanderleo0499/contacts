import { css } from "@emotion/react";

export const styleCursorPointer = css({
  cursor: "pointer",
});

export const styleFlexItemCenter = css({
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  "> * + *": {
    marginLeft: "0.5rem",
  },
});

export const styleFlexInitial = css({
  flex: "var(--flex-initial)",
});

export const styleFlex1 = css({
  flex: "var(--flex-1)",
});
