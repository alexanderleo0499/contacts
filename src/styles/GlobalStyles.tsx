import { Global, css } from "@emotion/react";
import { CSS_RESET } from "./constants";

const GlobalStyles = () => {
  return (
    <Global
      styles={css`
        ${CSS_RESET}
      `}
    />
  );
};

export default GlobalStyles;
