export const CSS_ROOT_VARIABLES = `
:root{
  --color-primary:black;
  --color-gray-50:#f9fafb;
  --color-gray-100:#F3F4F6;
  --color-gray-800:#1f2937;
  --color-slate-100:#f1f5f9;
  --color-slate-200:#e2e8f0;
  --color-slate-300:#cbd5e1;
  --color-slate-500:#64748b;
  --color-red-500:#ef4444;
  --size-rounded:0.25rem;
  --time-animation:0.2s;
  --shadow: 0 1px 3px 0 rgb(0 0 0 / 0.1), 0 1px 2px -1px rgb(0 0 0 / 0.1);
  --flex-initial:0 1 auto;
  --flex-1:1 1 0%;
}`;
export const CSS_RESET = `
@import url("https://fonts.googleapis.com/css2?family=Montserrat:wght@500&display=swap");

${CSS_ROOT_VARIABLES}

*,
*::before,
*::after {
  box-sizing: border-box;
}

* {
  margin: 0;
}

body {
  line-height: 1.5;
  -webkit-font-smoothing: antialiased;
}

img,
picture,
video,
canvas,
svg {
  display: block;
  max-width: 100%;
}

input,
button,
textarea,
select {
  font: inherit;
}

p,
h1,
h2,
h3,
h4,
h5,
h6 {
  overflow-wrap: break-word;
}
  
#root {
  isolation: isolate;
}

body {
    font-family: "Montserrat", "sans-serif";
    min-height:100vh;
    width:100%;
  }
@media only screen and (min-width: 800px) {
  html{
    background:var(--color-slate-100)
  }
    body {
       width:25rem;
       margin:0 auto;
       border:1px solid var(--color-slate-100);
       box-shadow:var(--shadow);
       background-color:white
    }
    
  }
`;
