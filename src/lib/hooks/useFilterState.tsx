import { useEffect, useState } from "react";
import { INIITAL_PAGE, INITIAL_PAGE_SIZE } from "../constants";
import { splitArrayToChunks, calculateCeilTotalPaginateItem } from "../utils";

interface UseFilterState {
  srcData: any;
  searchKey?: string;
  isPagination?: boolean;
}

const useFilterState = ({
  srcData,
  searchKey = "",
  isPagination = false,
}: UseFilterState) => {
  const [renderedData, setRenderedData] = useState<any | null>(null);
  const [pageSize, setPageSize] = useState<number>(INITIAL_PAGE_SIZE);
  const [selectedPage, setSelectedPage] = useState<number>(INIITAL_PAGE);
  const [allFilteredData, setAllFilteredData] = useState<any | null>(null);

  useEffect(() => {
    if (srcData) setRenderedData(srcData);
  }, [srcData]);

  useEffect(() => {
    if (!srcData) return;
    let filteredData: any = srcData;

    filteredData = srcData;

    if (!isPagination) return setRenderedData(filteredData);

    const totalPagination = calculateCeilTotalPaginateItem(
      filteredData.length,
      pageSize
    );

    if (selectedPage > totalPagination) setSelectedPage(1);

    const chunks = splitArrayToChunks(filteredData, pageSize);
    const rendered =
      chunks && chunks[selectedPage - 1] ? chunks[selectedPage - 1] : [];
    setRenderedData(rendered);
    setAllFilteredData(filteredData);
  }, [srcData, searchKey, pageSize, selectedPage, isPagination]);

  return {
    renderedData,
    pageSize,
    setPageSize,
    selectedPage,
    setSelectedPage,
    allFilteredData,
  };
};

export default useFilterState;
