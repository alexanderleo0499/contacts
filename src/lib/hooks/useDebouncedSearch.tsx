import { useEffect, useState } from "react";

const useDebounceSearch = (debouncedTime: number) => {
  const [search, setSearch] = useState<string>("");
  const [usedSearch, setUsedSearch] = useState<string>("");
  const [timeoutId, setTimeoutId] = useState<any>();

  //debounced effect
  useEffect(() => {
    const timeout = setTimeout(() => {
      setUsedSearch(search);
    }, debouncedTime);
    clearTimeout(timeoutId);
    setTimeoutId(timeout);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [search]);

  return { usedSearch, search, setSearch };
};
export default useDebounceSearch;
