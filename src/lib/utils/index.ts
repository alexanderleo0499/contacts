export const findUniqueObjectBetweenSameArrayModel = (
  arr1: any[],
  arr2: any[]
) => {
  const unique1 = arr1.filter((o) => arr2.indexOf(o) === -1);
  const unique2 = arr2.filter((o) => arr1.indexOf(o) === -1);

  const unique = unique1.concat(unique2);
  return unique;
};

export const calculateCeilTotalPaginateItem = (
  totalItem: number,
  pageSize: number
) => {
  return Math.ceil(totalItem / pageSize);
};

export const splitArrayToChunks = (array: Array<any>, size: number) => {
  return array.reduce((resultArray, item, index) => {
    const chunkIndex = Math.floor(index / size);

    if (!resultArray[chunkIndex]) {
      resultArray[chunkIndex] = []; // start a new chunk
    }

    resultArray[chunkIndex].push(item);

    return resultArray;
  }, []);
};
