import { Route } from "@src/typings/routes";
import homeRoutes from "./home.routes";

const Routes: Route[] = [...homeRoutes];

export default Routes;
