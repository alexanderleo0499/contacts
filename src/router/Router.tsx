import React, { Suspense } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import routes from "./routes";
import Loader from "@src/components/loader";

const Router: React.FC = () => {
  return (
    <BrowserRouter>
      <Routes>
        {routes.map(({ component: Component, path }) => {
          return (
            <Route
              key={path}
              element={
                <Suspense
                  fallback={
                    <Loader isLoading={true}>Load Application...</Loader>
                  }
                >
                  <Component />
                </Suspense>
              }
              path={path}
            />
          );
        })}
      </Routes>
    </BrowserRouter>
  );
};
export default Router;
