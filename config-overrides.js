const rewireAliases = require('react-app-rewire-aliases')
const path = require('path')
const { addBabelPreset, override , addWebpackAlias} = require('customize-cra')


module.exports = override(
    addBabelPreset('@emotion/babel-preset-css-prop'),
    addWebpackAlias({
        ['@src']:path.resolve(__dirname,'./src')
    })
)

